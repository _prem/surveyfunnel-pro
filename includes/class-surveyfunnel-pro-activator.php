<?php
/**
 * Fired during plugin activation
 *
 * @link       https://club.wpeka.com
 * @since      1.0.0
 *
 * @package    Surveyfunnel_Pro
 * @subpackage Surveyfunnel_Pro/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Surveyfunnel_Pro
 * @subpackage Surveyfunnel_Pro/includes
 * @author     WPeka Club <support@wpeka.com>
 */
class Surveyfunnel_Pro_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		update_option( 'surveyfunnel_pro_activated', true );
	}

}
