export default function AddCondition(props) {
	const {addConditionToState, buttonClass} = props;
	return (
		<button className={buttonClass ? buttonClass : ''} onClick={addConditionToState}>Add Condition</button>
	);
}