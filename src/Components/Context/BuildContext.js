const { addFilter } = wp.hooks;

addFilter('deleteItemInList', 'BuildContext' ,deleteItemInList);

function deleteItemInList(newList, data) {
	for(let i=0; i<newList.length; i++) {
		let conditions = JSON.parse( JSON.stringify(newList[i].conditions));
		conditions = conditions.filter((condition) => {
			return condition.jumpQuestion.id !== data.id;
		})
		newList[i].conditions = conditions;
	}
	return newList;
}