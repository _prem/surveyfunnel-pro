/**
 * @jest-environment jsdom
 */
import testFunctions from "../Survey";
import { scoringList } from './List.data.js';
import React from 'react';

function ShowErrors() {
	return <div></div>
}

test ( 'render checkbox in front end on cover page of survey', () => {
	let data = '';
	let configure = {
		proSettings :{
			privacyPolicy: {
				text: 'something',
				link: {
					value: 'http://www.google.com'
				}
			}
		}
	};
	let state = {
		privacyPolicy: true,
	}
	let jsx = testFunctions.renderPrivacyPolicyOption(data, configure, state);

	expect(jsx.props.children.type === 'div').toBeTruthy();
	state.privacyPolicy = false;
	jsx = testFunctions.renderPrivacyPolicyOption(data, configure, state);
	expect(jsx.props.children === '').toBeTruthy();

	configure.proSettings.privacyPolicy.text = '';
	jsx = testFunctions.renderPrivacyPolicyOption(data, configure, state);
	expect(jsx.props.children === '').toBeTruthy();
} )

test( 'validation for cover page in front end', () => {

	let iframeRef = {
		current: {
			node: {
				contentDocument: {
					getElementById: function() {
						return {
							checked: true,
						}
					}
				}
			}
		}
	}

	let error = [];
	let item = {
		privacyPolicy: true,
	}

	let configure = {
		proSettings: {
			privacyPolicy: {
				text: 'something',
				link: {
					value: 'http://www.google.com'
				}
			}
		}
	}

	let flag = testFunctions.checkCoverPageButtonValidations(false, item, iframeRef, error, configure);
	expect(flag).toBeFalsy();
	expect(error.length === 0).toBeTruthy();

	iframeRef = {
		current: {
			node: {
				contentDocument: {
					getElementById: function() {
						return {
							checked: false,
						}
					}
				}
			}
		}
	}

	flag = testFunctions.checkCoverPageButtonValidations(false, item, iframeRef, error, configure);
	expect(flag).toBeTruthy();
	expect(error.length === 1).toBeTruthy();

	configure = {
		proSettings: {
			privacyPolicy: {
				text: ''
			}
		}
	};
	error = [];

	flag = testFunctions.checkCoverPageButtonValidations(false, item, iframeRef, error, configure);
	expect(flag).toBeFalsy();
	expect(error.length === 0).toBeTruthy();
} )

test ( 'render result elements', () => {
	let jsx = testFunctions.renderResultScreen( '', {}, 'basic', 0 );
	expect(jsx).toBe('');
	
	let item = {
		showResult: false,
		displayMessage: 'Your Score is',
	}
	let globalTotalScore = {
		score: 0,
	}
	jsx = testFunctions.renderResultScreen('',item, 'scoring', globalTotalScore );
	expect(jsx).toBe('');

	item.showResult = true;
	jsx = testFunctions.renderResultScreen( '', item, 'scoring', globalTotalScore );
	
	expect(jsx.props.className).toBe('showResult');
	expect(jsx.props.children[0]).toBe(item.displayMessage);
	
} );

test( 'calculate score and get result tab number to show on frontend.', () => {
	let globalTotalScore = {
		score: 0,
	}
	let num = testFunctions.changeCurrentTabAsPerSurveyType( 1, 'scoring', scoringList, 1, globalTotalScore );
	expect(num === 1).toBeTruthy();

	num = testFunctions.changeCurrentTabAsPerSurveyType( 1, 'basic', scoringList, 1, globalTotalScore );
	expect(num === 1).toBeTruthy();

	num = testFunctions.changeCurrentTabAsPerSurveyType( 1, 'scoring', scoringList, 8, globalTotalScore );
	expect(num === 1).toBeTruthy();

	num = testFunctions.changeCurrentTabAsPerSurveyType( 1, 'scoring', scoringList, 3, globalTotalScore );
	expect(num === 1).toBeFalsy();
	expect(globalTotalScore.score).toBe(40);

	scoringList[1].answers[1].score = 100;

	num = testFunctions.changeCurrentTabAsPerSurveyType( 1, 'scoring', scoringList, 3, globalTotalScore );
	expect(num === scoringList.length - 1).toBeTruthy();
} );

test( 'render content elements survey', () => {

	let jsx = testFunctions.renderContentElements(  '', {
		componentName: 'hello',
	}, {display: 'none'}, () => { return '' }, () => {}, {}, () => {}, 1, [], <div></div> );

	expect(jsx).toBe('');

	jsx = testFunctions.renderContentElements(  '', {
		componentName: 'ImageQuestion',
		answers: [
			{
				imageUrl: 'http://www.google.com/images/car.jpg'
			},
		]
	}, {display: 'none'}, () => { return '' }, () => {}, {}, () => {}, 1, [], ShowErrors );

	expect(jsx.props.className).toBe('surveyfunnel-lite-tab-ImageQuestion');

	jsx = testFunctions.renderContentElements(  '', {
		componentName: 'TextElement',
	}, {display: 'none'}, () => { return '' }, () => {}, {}, () => {}, 1, [], ShowErrors );

	expect(jsx.props.className).toBe('surveyfunnel-lite-tab-TextElement');

} );

test('calling render content elements as per element type', () => {
	let data = testFunctions.callRenderContentElements('', () => { return 'HelloWorld' }, {
		componentName: 'TextElement',
	}, 10);

	expect(data).toBe('HelloWorld');
	data = testFunctions.callRenderContentElements('', () => { return 'HelloWorld' }, {
		componentName: 'ImageQuestion',
	}, 10);

	expect(data).toBe('HelloWorld');

	data = testFunctions.callRenderContentElements('', () => { return 'HelloWorld' }, {
		componentName: 'SomethingElse',
	}, 10);

	expect(data).toBe('');
})

test( 'check validations', () => {
	let error = [];
	let data = testFunctions.checkValidations( '', {
		componentName: 'nothing'
	}, error );

	expect(data).toBe(undefined);

	data = testFunctions.checkValidations( '', {
		componentName: 'ImageQuestion',
		mandatory: false,
	}, error );

	expect(data).toBe(undefined);

	data = testFunctions.checkValidations( '', {
		componentName: 'ImageQuestion',
		mandatory: true,
		value: ''
	}, error );

	expect(error.length).toBe(1);
} )