import testFunction from '../../../../Components/Build/BuildElements/ResultScreenElements.js';

test( 'modify result screen state if surveytype is scoring', () => {
	let data = testFunction.resultScreenState({}, 'basic');
	expect(data).toEqual({});

	data = testFunction.resultScreenState( {}, 'scoring' );
	expect(data).toEqual({
		startRange: 0,
		endRange: 0,
		showResult: false,
		displayMessage: 'Your Score is',
	})

} );

test( 'set mounting data in result state function', () => {
	let currentELement = {
		startRange: 0,
		endRange: 20,
		showResult: false,
		displayMessage: 'message'
	};

	expect( testFunction.resultScreenSetComponentMount({}, 'basic', currentELement) ).toEqual({});
	expect( testFunction.resultScreenSetComponentMount({}, 'scoring', currentELement) ).toEqual(currentELement);
	currentELement = {};

	expect( testFunction.resultScreenSetComponentMount({}, 'scoring', currentELement) ).toEqual({
		startRange: 0,
		endRange: 0,
		showResult: false,
		displayMessage: 'Your Score is'
	});
})

test( 'Result Screen Left Scoring Settings Render test', () => {
	let jsx = testFunction.resultScreenLeftElementsRender('', 'basic', {}, () => {});
	expect(jsx).toBe('');

	let state = {
		startRange: 0,
		endRange: 20,
		showResult: false,
		displayMessage: 'message'
	};

	jsx = testFunction.resultScreenLeftElementsRender('', 'scoring', state, () => {} );
	expect(jsx.type).toBe('div');
} );

test( 'Result Screen Right elements render test', () => {
	let jsx = testFunction.resultScreenRightElementsRender( '', 'basic', {} );
	expect(jsx).toBe('');
	let state = {
		showResult: false,
		displayMessage: 'HelloWorld',
	}
	jsx = testFunction.resultScreenRightElementsRender('', 'scoring', state);
	expect(jsx).toBe('');
	state.showResult = true;
	jsx = testFunction.resultScreenRightElementsRender('', 'scoring', state);
	expect(jsx.type).toBe('div');
} )

test ( "Result Screen Validations", () => {
	let errorArray = [];
	let state = {
		id: 'helloworld',
		startRange: 20,
		endRange: 10,
	}
	testFunction.resultScreenValidation(errorArray, state, []);
	expect(errorArray.length === 1).toBeTruthy();
	expect(errorArray[0].props.children).toBe('Start Range cannot be greater than end range');

	state.startRange = 10;
	state.endRange = 20;
	errorArray = [];
	let List = {
		RESULT_ELEMENTS: [{
			id: 'helloworld',
		},{
			id: 'nothing',
			startRange: 15,
			endRange: 30,
		}]
	}
	testFunction.resultScreenValidation(errorArray, state, List);
	expect(errorArray.length === 1).toBeTruthy();
	expect(errorArray[0].props.children).toBe('The max score range is overlapping with the score set on another result page');
	errorArray = [];
	List = {
		RESULT_ELEMENTS: [{
			id: 'helloworld',
		},{
			id: 'nothing',
			startRange: 0,
			endRange: 15,
		}]
	}
	testFunction.resultScreenValidation(errorArray, state, List);
	expect(errorArray.length === 1).toBeTruthy();
	expect(errorArray[0].props.children).toBe('The min score range is overlapping with the score set on another result page');
} );